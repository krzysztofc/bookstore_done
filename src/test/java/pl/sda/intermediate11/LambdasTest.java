package pl.sda.intermediate11;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LambdasTest {

    @FunctionalInterface
    public interface SuperChecker {
        boolean check(Integer value);
    }

    private class OddChecker implements SuperChecker {


        @Override
        public boolean check(Integer value) {
            return value % 2 != 0;
        }
    }

    @Test
    void checkerTest() {
        OddChecker oddChecker = new OddChecker();
        Assert.assertTrue(oddChecker.check(3));

        SuperChecker superChecker = new SuperChecker() {
            @Override
            public boolean check(Integer value) {
                return value % 2 != 0;
            }
        };

        Assert.assertTrue(superChecker.check(3));

        SuperChecker lambdaSuperChecker = e -> e % 2 != 0;

    }

    @FunctionalInterface
    public interface MathOperation {
        int operation(int a, int b);

        default String message() {
            return "BUM!";
        }
    }

    @Test
    void mathOperationTest() {

        MathOperation addition = (a, b) -> a + b;
        MathOperation subtraction = (a, b) -> a - b;
        MathOperation multiplication = (a, b) -> a * b;
        MathOperation division = (a, b) -> a / b;

        Assert.assertEquals(5,addition.operation(2,3));
        Assert.assertEquals(-1,subtraction.operation(2,3));
        Assert.assertEquals(6,multiplication.operation(2,3));
        Assert.assertEquals(0,division.operation(2,3));
    }

    @FunctionalInterface
    public interface MyBiComparator<T,U>{
        int biCOmpare(T obj1, U obj2);
    }

    @Test
    void compareTwoTypes() {
        Integer number = 20;
        String text = "123";

        MyBiComparator<Integer, String> myBiComparatorTextFirst =
                (a, b) -> a.toString().compareTo(b);
        Assertions.assertEquals(1, myBiComparatorTextFirst.biCOmpare(number, text));
    }
}
