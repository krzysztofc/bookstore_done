package pl.sda.intermediate11;

import org.junit.jupiter.api.Test;

import java.util.concurrent.CompletableFuture;

public class ThreadsTest {

    @Test
    void threads() throws InterruptedException {

        Thread customProcesThread = new CustomProcesThread(); // w ten sposob od razu tworzymy wątek
        customProcesThread.start();

        Thread thread = new Thread(new CustomProcesRunnable()); // w przypadku runnable musimy jescze utowrzyć wątek
        thread.start();

        customProcesThread.join();
        thread.join();


//        CompletableFuture.supplyAsync(()->)
    }
}
