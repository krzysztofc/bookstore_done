package pl.sda.intermediate11;

import org.junit.jupiter.api.Test;
import pl.sda.intermediate11.bookstore.StringCalculator;

import static org.junit.jupiter.api.Assertions.*;

class StringCalculatorTest {

    @Test
    void shouldReturnZeroWhenTextIsEmpty() {
        String exampleData = "";

        int result = StringCalculator.adding(exampleData);

        assertEquals(0, result);
    }

    @Test
    void shouldReturnZeroWhenTextIsBlank() {
        String exampleData = " ";

        int result = StringCalculator.adding(exampleData);

        assertEquals(0, result);
    }

    @Test
    void shouldReturnNumberWhenDataIsOneNumber() {
        String exampleData1 = "7";
        String exampleData2 = "7";

        int result1 = StringCalculator.adding(exampleData1);
        int result2 = StringCalculator.adding(exampleData2);

        assertEquals(7, result1);
        assertEquals(7, result2);
    }

    @Test
    void shouldReturnSumWhenTextHasTwoNumbersWithDelimiter() {
        String exampleData = " 1   ,  2 ";

        int result = StringCalculator.adding(exampleData);

        assertEquals(3, result);
    }

    @Test
    void shouldReturnSumWhenTextHasMoreThanTwoNumbersWithDelimiter() {
        String exampleData = " 1   ,  2 , 5 ,7,,,,";

        int result = StringCalculator.adding(exampleData);

        assertEquals(15, result);
    }

    @Test
    void shouldReturnSumWhenTextHasMoreThanTwoNumbersWithDelimiterOrNewLine() {
        String exampleData = " 1   ,  2 , 5 \n7,,,,";

        int result = StringCalculator.adding(exampleData);

        assertEquals(15, result);
    }

    @Test
    void shouldReturnSumOfManyNumbersSeparatedWithCustomDelimiter() {
        String exampleData = "//;\n1;2";

        int result = StringCalculator.adding(exampleData);

        assertEquals(3, result);
    }


}