package pl.sda.intermediate11.bookstore.products;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

class ProductsDAOTest {

    @Test
    void shouldPopulateProductList() {
        Product product = new Product("Liika viisas, by Maiju Lassila", 16365);
        product.setDescription("[Subtitle: Viisaudenkirja eli kertomus Sakari Kolistajasta]" + "[Language: Finnish]");

        ProductsDAO productsDAO = new ProductsDAO();
        List<Product> productsList = productsDAO.getProductsList("", Integer.MAX_VALUE);

        Product productToMatch = productsList.stream()
                .filter(p -> p.getId() == 16365)
                .findFirst()
                .orElseThrow(()->new RuntimeException("Nie znaleziono produktu"));

        Assertions.assertEquals(productToMatch.getDescription(),product.getDescription());
        Assertions.assertEquals(productToMatch.getTitle(),product.getTitle());
    }

}