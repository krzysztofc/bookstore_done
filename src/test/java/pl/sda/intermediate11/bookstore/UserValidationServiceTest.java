package pl.sda.intermediate11.bookstore;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pl.sda.intermediate11.bookstore.users.entities.UserAddress;
import pl.sda.intermediate11.bookstore.users.dtos.UserRegistrationDTO;
import pl.sda.intermediate11.bookstore.users.services.UserValidationService;

import java.util.Map;

class UserValidationServiceTest {

    @Test
    void shouldNotPassUserWithBlankFirstName() {

        UserRegistrationDTO userRegistrationDTO = populateValidUser();
        userRegistrationDTO.setFirstName(" ");

        UserValidationService userValidationService = new UserValidationService();
        Map<String, String> errorsMap = userValidationService.validateUserData(userRegistrationDTO);

        Assertions.assertTrue(errorsMap.containsKey(UserValidationService.FIRST_NAME_VAL_RES));
    }

    @Test
    void shouldNotPassUserWithToShortFirstName() {

        UserRegistrationDTO userRegistrationDTO = populateValidUser();
        userRegistrationDTO.setFirstName("An     ");

        UserValidationService userValidationService = new UserValidationService();
        Map<String, String> errorsMap = userValidationService.validateUserData(userRegistrationDTO);

        Assertions.assertTrue(errorsMap.containsKey(UserValidationService.FIRST_NAME_VAL_RES));
    }

    private UserRegistrationDTO populateValidUser() {
        UserRegistrationDTO userRegistrationDTO = new UserRegistrationDTO();
        UserAddress userAddress = new UserAddress();
        userAddress.setCity("New York");
        userAddress.setCountry("USA");
        userAddress.setStreet("Akacjowa");
        userAddress.setZipCode("12-345");

        userRegistrationDTO.setFirstName("Anna");
        userRegistrationDTO.setLastName("Nowak");
        userRegistrationDTO.setBirthDate("1990-04-11");
        userRegistrationDTO.setPesel("12345678901");
        userRegistrationDTO.setEmail("a.nowak@gmail.com");
        userRegistrationDTO.setPhone("123-456-789");
        userRegistrationDTO.setPassword("annanowak123456");
        userRegistrationDTO.setUserAddress(userAddress);

        return userRegistrationDTO;
    }
}