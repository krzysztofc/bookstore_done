package pl.sda.intermediate11.threads;

import com.google.common.collect.Lists;
import org.junit.jupiter.api.Test;

import java.util.List;

public class ThreadsExample {

    @Test
    void bankWithThreads() {
        List<BankClientAction> clientActions = Lists.newArrayList();
        for (int i = 0; i < 1000; i++) {
            clientActions.add(new BankClientAction());
        }

        List<Thread> threads = Lists.newArrayList();
        for (BankClientAction clientAction : clientActions) {
            threads.add(new Thread(clientAction));
        }

        for (Thread thread : threads) {
            thread.start(); // uruchamiamy zadania
        }

        for (Thread thread : threads) {
            try {
                thread.join(); //czekamy na zakonczenie pracy
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Ilosc operacji: " + Bank.operations);
        System.out.println("Stan konta: " + Bank.balance);
    }

    @Test
    void runnableBasics() {

        Runnable ourRunnable = new OurRunnable(); // zadanie

        Runnable anonymous = new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName() + " Anonimowa klasa!");
            }
        };

        Runnable lambdaRunnable = () -> System.out.println(Thread.currentThread().getName() + " Lambda!"); // inna forma zapisu tego co wyzej, przepis na wypisanie "lambda" nie przyjmuje niczego a robi sout


        ourRunnable.run();
        anonymous.run();
        lambdaRunnable.run();

        Thread firstThread = new Thread(ourRunnable);
        Thread secondThread = new Thread(anonymous);
        Thread thirdThread = new Thread(lambdaRunnable);

        List<Thread> threads = Lists.newArrayList(firstThread, secondThread, thirdThread);

        for (Thread thread : threads) {
            thread.start();
        }
    }
}
