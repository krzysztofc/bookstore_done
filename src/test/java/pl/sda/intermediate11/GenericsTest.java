package pl.sda.intermediate11;

import com.google.common.collect.Lists;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class GenericsTest {

    @Test
    void test1() {
        printAndSort(Lists.newArrayList("bca", "abc", "cba"), (a, b) -> a.compareTo(b));
        printAndSort(Lists.newArrayList(5, 1, 3, "0"), (a, b) -> a.toString().compareTo(b.toString()));
        sumOfNumbersParam(Lists.newArrayList(1, 2, 3, 4, 5, 6, 7, 8), (n -> n > 5));

        Pair<Integer, String> pair1 = new Pair<>();
        Pair<Integer, String> pair2 = new Pair<>();
        pair1.setObj1(1);
        pair2.setObj1(1);
        pair1.setObj2("2");
        pair2.setObj2("2");
        System.out.println(pair1.equals(pair2));
    }

    private <E> void printEverything(List<E> list) {
        for (E e : list) {
            System.out.printf(e.toString());
        }
    }

    private <E extends Comparable> void printAndSort(List<E> list, Comparator<E> comparator) {
        list.stream().sorted(comparator)
                .forEach(e -> System.out.println(e));
    }

    private <E extends Number> void sumOfNumbers(List<E> list) {
        int sum = 0;
        for (E e : list) {
            sum = sum + e.intValue();
        }
    }

    private <E extends Number> void sumOfNumbersParam(List<E> list, Predicate<Integer> integerPredicate) { // predykat np. n-> n>4
        list.stream()
                .map(e -> e.intValue())
                .filter(integerPredicate)
                .reduce((a, b) -> a + b)
                .orElse(0);
    }

    private boolean compareToPairs(Pair pair1, Pair pair2) {
        return pair1.equals(pair2);
    }

}

////Generyki
//Napisz generyczną metodę która przyjmuje Listę jakichkolwiek elementów
// i wypisuje wszystkie elementy tej listy
//
//Napisz generyczną metodę która przyjmuje Listę jakichkolwiek elementów
// i wypisuje wszystkie elementy tej listy, ale z zachowaniem kolejności przekazanej w parametrze
//
//Napisz generyczną metodę która przyjmuje Listę elementów będących numerami
// i zwraca ich sumę
//
//Napisz generyczną metodę która przyjmuje Listę elementów będących numerami
// i zwraca ich sumę, ale fitruje elementy większe od dodatkowo przekazanego parametru
//
//Napisz klasę generyczną klasę Pair przechowującą dwa elementy różnych typów (T,E)
//
//Napisz metodę która porówna dwa obiekty klasy Pair (equals)
