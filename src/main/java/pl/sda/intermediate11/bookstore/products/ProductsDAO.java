package pl.sda.intermediate11.bookstore.products;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class ProductsDAO {

    List<Product> productsList = new ArrayList<>();

    public List<Product> getProductsList(String searchText, int maxSize) {
        if (productsList.isEmpty()) {
            productsList = populateProductsList();
        }
        if (StringUtils.isBlank(searchText)) {
            return productsList.stream()
                    .limit(maxSize)
                    .collect(Collectors.toList());
        }
        return productsList.stream()
                .filter(n -> n.getTitle().contains(searchText))
                .limit(maxSize)
                .collect(Collectors.toList());
    }

    private List<Product> populateProductsList() {
        Pattern pattern = Pattern.compile("^([\\w]+.+[\\w\\.\\?!])(\\W+)([1-9]+)$");
        List<Product> products = Lists.newArrayList();
        try {
            List<String> lines = Files.readAllLines(Paths.get(this
                    .getClass().getClassLoader().getResource("GUTINDEX.ALL").toURI()));
            boolean readingProductsActive = false;
            boolean readAdditionasDescription = false;
            Product product = null;
            for (String line : lines.stream().map(l->l.trim()).collect(Collectors.toList())) {
                if (line.startsWith("TITLE and AUTHOR")) {
                    readingProductsActive = true;
                }
                if (line.startsWith("<==End of GUTINDEX.ALL==>")) {
                    readingProductsActive = false;
                }
                Matcher matcher = pattern.matcher(line);
                if (matcher.matches() && readAdditionasDescription) {
                    readAdditionasDescription = false;
                }
                if (readingProductsActive && matcher.matches()) {
                    String title = matcher.group(1);
                    Integer id = Integer.parseInt(matcher.group(3));
                    product = new Product(title, id);
                    products.add(product);
                    readAdditionasDescription = true;
                }
                if (readAdditionasDescription && readingProductsActive && !matcher.matches() && StringUtils.isNotBlank(line)) {
                    if (StringUtils.isBlank(product.getDescription())) {
                        product.setDescription(line);
                    } else {
                        product.setDescription(product.getDescription() + line);
                    }
                }
            }
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        return products;
    }
}
