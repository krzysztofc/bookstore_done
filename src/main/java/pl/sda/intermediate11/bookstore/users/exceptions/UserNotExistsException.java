package pl.sda.intermediate11.bookstore.users.exceptions;

public class UserNotExistsException extends RuntimeException {


    public UserNotExistsException(String message) {
        super(message);
    }
}
