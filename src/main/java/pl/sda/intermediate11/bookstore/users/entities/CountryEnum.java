package pl.sda.intermediate11.bookstore.users.entities;

import lombok.Getter;

@Getter
public enum CountryEnum {
    POLAND("PL", "Polska"), //to sa singletony
    GERMANY("DE", "Niemcy"),
    ENGLAND("ENG", "Anglia"),
    FRANCE("FR", "Francja"),
    RUSSIA("RU", "Rosja"),
    USA("US", "Stany Zjednoczone");

    private String symbol;
    private String plName;

    CountryEnum(String symbol, String plName) {
        this.symbol = symbol;
        this.plName = plName;
    }
}
